# eslint-config-nomo

This package provides ESLint rules as an [extensible shared config](http://eslint.org/docs/developer-guide/shareable-configs) for JavaScript projects.

## Usage

Once the `eslint-config-nomo` package is installed, you can use it by specifying `nomo` in the [`extends`](http://eslint.org/docs/user-guide/configuring#extending-configuration-files) section of your [ESLint configuration](http://eslint.org/docs/user-guide/configuring).

```js
{
  "extends": "nomo",
  "rules": {
    // Additional, per-project rules...
  }
}
```
