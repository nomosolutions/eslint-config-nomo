module.exports = {
  rules: {
    // enforce spacing inside array brackets
    'array-bracket-spacing': ['error', 'never'],

    // enforce consistent spacing inside single-line blocks
    'block-spacing': ['error', 'always'],

    // enforce consistent brace style for blocks
    'brace-style': ['error', '1tbs', { allowSingleLine: true }],

    // enforce camelcase naming convention
    camelcase: ['error', { properties: 'never' }],

    // enforce or disallow capitalization of the first letter of a comment
    'capitalized-comments': ['off', 'always', {
      line: {
        ignorePattern: '.*',
        ignoreInlineComments: true,
        ignoreConsecutiveComments: true,
      },
      block: {
        ignorePattern: '.*',
        ignoreInlineComments: true,
        ignoreConsecutiveComments: true,
      },
    }],

    // require or disallow trailing commas
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],

    // enforce consistent spacing before and after commas
    'comma-spacing': ['error', { before: false, after: true }],

    // enforce consistent comma style
    'comma-style': ['error', 'last'],

    // enforce consistent spacing inside computed property brackets
    'computed-property-spacing': ['error', 'never'],

    // enforce consistent naming when capturing the current execution context
    'consistent-this': ['error', 'that'],

    // require or disallow newline at the end of files
    'eol-last': ['error', 'always'],

    // require or disallow spacing between function identifiers and their invocations
    'func-call-spacing': ['error', 'never'],

    // require function names to match the name of the variable or property to
    // which they are assigned
    'func-name-matching': ['error', 'always', {
      includeCommonJSModuleExports: false,
    }],

    // require or disallow named function expressions
    'func-names': ['warn', 'as-needed'],

    // enforce the consistent use of either function declarations or expressions
    'func-style': ['warn', 'declaration', { allowArrowFunctions: true }],

    // disallow specified identifiers
    'id-blacklist': ['off'],

    // enforce minimum and maximum identifier lengths
    'id-length': ['off', {
      min: 1,
      max: Infinity,
      properties: 'never',
    }],

    // require identifiers to match a specified regular expression
    'id-match': ['off'],

    // enforce consistent indentation
    indent: ['error', 2, {
      SwitchCase: 1,
      VariableDeclarator: 1,
      outerIIFEBody: 1,
      MemberExpression: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1,
      },
      FunctionExpression: {
        parameters: 1,
        body: 1,
      },
      CallExpression: {
        parameters: 1,
      },
      ArrayExpression: 1,
      ObjectExpression: 1,
    }],

    // enforce the consistent use of either double or single quotes in JSX attributes
    'jsx-quotes': ['off', 'prefer-double'],

    // enforce consistent spacing between keys and values in object literal properties
    'key-spacing': ['error', {
      beforeColon: false,
      afterColon: true,
      mode: 'minimum',
    }],

    // enforce consistent spacing before and after keywords
    'keyword-spacing': ['error', {
      before: true,
      after: true,
    }],

    // disallow dangling underscores in identifiers
    'no-underscore-dangle': ['off'],

    // enforce the consistent use of either backticks, double, or single quotes
    quotes: ['error', 'single', { avoidEscape: false }],
  },
};
