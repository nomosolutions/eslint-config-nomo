module.exports = {
  env: {
    node: true,
  },

  rules: {
    // enforce return after callback
    'callback-return': 'error',

    // enforce require() on the top-level module scope
    'global-require': 'error',

    // enforce callback error handling
    'handle-callback-err': 'error',

    // disallow require calls to be mixed with regular variable declarations
    'no-mixed-requires': 'error',

    // disallow new require
    'no-new-require': 'error',

    // disallow string concatenation when using __dirname and __filename
    'no-path-concat': 'error',

    // disallow process.env
    'no-process-env': 'off',

    // disallow process.exit()
    'no-process-exit': 'off',

    // disallow Node.js modules
    'no-restricted-modules': 'off',

    // disallow synchronous methods
    'no-sync': 'error',
  },
};
